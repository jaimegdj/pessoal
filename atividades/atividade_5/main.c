#include <stdio.h>
#include <string.h>
#include "pilha.h"

int main (){
	Pilha fila, *p=&fila;
	char ex[] = "(2+3)*5";
	int i;
	inicializa(p);
	for(i=0; i<strlen(ex); i++){
		if(ex[i] == "+" || ex[i] == "-" ex[i] == "*" || ex[i] == "/")
			push(p, ex[i]);
		else if(ex[i] == "(")
			printf("%c", pop(p));
		else
			printf("%c", ex[i]);
	}
	return 0;
}
