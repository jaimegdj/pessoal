#define TAMANHO 10

struct pilha{
	char pilha[TAMANHO];
	char topo;
};

typedef struct pilha Pilha;

void inicializa (Pilha *p);

void push(Pilha *p, char elem);

char pop(Pilha *p);



