#define TAMANHO 5

struct pilha{
	int pilha[TAMANHO];
	int topo;
};

typedef struct pilha Pilha;

void inicializa (Pilha *p);

void inserir (Pilha *p, int elem);

void remover(Pilha *p);

void imprimirTopo(Pilha *p);

Pilha *removerBase(Pilha *p);
