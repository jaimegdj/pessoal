#include <stdio.h>
#include "pilha.h"

//inicializa como no indicado da questao com topo -1.
void inicializa (Pilha *p){
	p->topo = -1;
}
//Usuario insere elementos na pilha
void inserir(Pilha *p, int elem){
	if(p->topo < TAMANHO-1){
		p->topo++;
		p->pilha[p->topo] = elem;
	}
	else
		printf("A PILHA ESTA CHEIA\n");
}
//Retirada de elementos da pilha
void remover(Pilha *p){
	if(p->topo != -1)
		p->topo--;
	else
		printf("PILHA VAZIA!!\n");
}

//Imprime o topo da pilha.
void imprimirTopo (Pilha *p){
	if((p->topo)!=-1){
		printf("Valor do topo da pilha eh:%d \n",p->pilha[(p->topo)]);
	}
	else{
		printf("A PILHA ESTÁ VAZIA.\n");
	}
}

Pilha *removerBase(Pilha *p){
	int aux[TAMANHO], topoAux, i;
	topoAux = p->topo;
	while(p->topo > 0){
		aux[p->topo-1] = p->pilha[p->topo];
		p->topo--;
	}
	inicializa(p);
	for (i=0; i<topoAux-1; i++){
		p->pilha[p->topo] = aux[i];
		p->topo++;
	}
	return p;
}
