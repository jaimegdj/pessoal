#include <stdio.h>
#include "pilha.h"

int main (){
	int flag = 1;
	Pilha fila, *p=&fila;
	
	inicializa(p);
	
	while(flag){
		int in;
		printf("1 - Incerir valor.\n");
		printf("2 - Imprimir topo.\n");
		printf("3 - Remover.\n");
		printf("4 - Remover base da pilha.\n");
		printf("Selecione uma opcao: ");
		scanf("%d", &in);
		switch(in){
			int aux;
			case 1:
				printf("Diga um valor a ser inserido: ");
				scanf("%d", &aux);
				inserir(p,aux);
			break;
			
			case 2:
				imprimirTopo(p);
			break;
			
			case 3:
				remover(p);
			break;
			
			case 4:
				p = removerBase(p); //bugada
			break;
			
			default:
				printf("Valor invalido!");
			break;
		}
	}
	
	return 0;
}
