#include "game.h"
#define TAM 5

typedef struct pilha {
	Game fila[TAM];
	int topo;
}Pilha;

void inserir(Pilha *p, Game *game);
void verUltimoJogo(Pilha *p);
void excluirTopo(Pilha *p);
