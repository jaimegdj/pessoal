#include<stdio.h>
#include"game.h"

Game *inserirJogo (Game *p){
	printf("Insira o nome do jogo: ");
	fgets(p->name, 10, stdin);
	return p;
}

/*
a função abaixo organiza o vetor de jogos de acordo com a avaliação que ele recebeu, usando BOLHA

Game *listarJogosRating (Game *a){
	Game aux;
	int i, j;
	for(i=0; i<K; i++){
		for(j=i+1; j<K; j++){
			if( (a+i)->rating < (a+j)->rating ){
				aux = *(a+i);
				*(a+i) = *(a+j);
				*(a+j) = aux;
			}
		}
	}
	return a; // retorna o ponteiro do vetor de jogos organizado por avaliação
}
/*
a função abaixo organiza o vetor de jogos de acordo com a categoria, usando BOLHA

Game *listarJogosCateg (Game *a){
	Game aux;
	int i,j;
	for(i=0; i<K; i++){
		for(j=i+1; j<K; j++){
			if(strcmp((a+i)->name, (a+j)->name) > 0){
				aux = *(a+j);
				*(a+j) = *(a+i);
				*(a+i) = aux;
			}
		}
	}
	return a;// retorna o ponteiro do vetor de jogos organizado por categoria
}
/*
a função abaixo organiza o vetor de jogos de acordo com a data de lançamento, usando BOLHA

Game *listarJogosRelease (Game *a){
	Game aux;
	int i,j;
	for(i=0; i<K; i++){
		for(j=i+1; j<K; j++){
			if((a+i)->release < (a+j)->release){
				aux = *(a+i);
				*(a+i) = *(a+j);
				*(a+j) = aux;
			}
		}
	}
	return a;// retorna o ponteiro do vetor de jogos organizado por lançamento
}
*/
