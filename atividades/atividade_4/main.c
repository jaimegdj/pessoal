#include <stdio.h>
#include "pilhaJogos.h"

int main (){
	Pilha pl, *pPi = &pl;
	Game jogo, *pJogo = &jogo;
	int flag=1;
	
	while(flag){
		int in;
		printf("1 - Incerir Jogo.\n");
		printf("2 - Imprimir topo.\n");
		printf("3 - Remover.\n");
		printf("4 - Sair.\n");
		printf("Selecione uma opcao: ");
		scanf("%d", &in);
		
		switch(in){
			case 1:
				pJogo = inserirJogo(pJogo);
				inserir(pPi, pJogo);
			break;
			
			case 2:
				verUltimoJogo(pPi);
			break;
			
			case 3:
				excluirTopo(pPi);
			break;
			
			case 4:
				in = 0;
			break;
			
			default:
				printf("OPCAO INVÁLIDA! ");
			break;
		}
	}
	
	return 0;
}
