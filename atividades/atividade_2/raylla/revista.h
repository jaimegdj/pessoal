typedef struct revista{
	string titulo[100];
	string genero[100];
	float preco;
}Revista;

void cria(Revista *r, string titulo[100], string genero[100], float preco); 
void ver(Revista *r);
void comprar(Revista *r);
void ler(Revista *r);
