#include <stdio.h>
#include <stlib.h>
#include "revista.h"
#include <locale.h>

int main(){
    setlocale(LC_ALL, "Portuguese");
    
	Revista r, *p=&r;

	cria(p);
	ver(p);
	comprar(p);
    ler(p);
}
