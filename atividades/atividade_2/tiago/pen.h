typedef struct Pen {
  float price;
  float tint;
  int color;
} Pen;

Pen create(Pen *p, float tint, int color, float price);
void getPrice(Pen *p);
void getTintVolume(Pen *p);
void getColor(Pen *p);
