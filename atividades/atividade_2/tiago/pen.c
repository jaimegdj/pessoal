#include <stdio.h>
#include "pen.h"

Pen create(Pen *p, float tint, int color, float price){
  p->price = price;
  p->color = color;
  p->tint = tint;
  
  return *p;
}
void getPrice(Pen *p){
  printf("O preco eh %.2f \n", p->price);
}
void getTintVolume(Pen *p){
  printf("O volume eh %.2f \n", p->tint);
}
void getColor(Pen *p){
  printf("A cor eh %d \n", p->color);
}
