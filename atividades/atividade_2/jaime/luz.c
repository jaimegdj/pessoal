#include <stdio.h>
#include <stdlib.h>
#include "luz.h"
Luz novaLuz (Luz *l, float tamanhoLuz, float valor, char tipo){
  l->tamanhoLuz = tamanhoLuz;
  l->valor = valor;
  l->tipo = tipo;

  return *l;
}
void tamanhoLuz(Luz *l){
  printf ("O tamanho da luz e %f \n", l->tamanhoLuz);
}
void valorLuz(Luz *l){
  printf("O valor da luz e %2.f \n", l->valor );
}
void tipoLuz(Luz *l){
  printf("O tipo da Luz e %c \n", l->tipo );
}
