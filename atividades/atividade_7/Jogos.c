#include "Jogos.h"
#include <stdio.h>
void init(Fila *f){
	f->inicio = 0;
	f->fim = -1;
}
void insere(Fila *f, Jogo jogo){
	if(f->fim == TAM-1){
		f->fim = -1;
	}
	
	if(f->fim != TAM-1){
		f->fim++;
		f->fila[f->fim] = jogo;
	}
}
void retira(Fila *f){
	f->inicio++;
	if(f->inicio == TAM)
		f->fim = 0;
}

void imprimir(Fila *f){
	int i = f->inicio;
	while(i != f->fim){
		printf("Jogo %s ano %d\n", f->fila[i].nome, f->fila[i].ano);
		i++;
		if(i > TAM-1)
			i=0;
	}
}
