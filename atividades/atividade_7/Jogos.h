#define TAM 5
typedef struct jogo{
	char nome[50];
	int ano;
}Jogo;
typedef struct fila{
	Jogo fila[TAM];
	int inicio, fim;
}Fila;

void init(Fila *f);
void insere(Fila *f, Jogo jogo);
void retira(Fila *f);
void imprimir(Fila *f);
