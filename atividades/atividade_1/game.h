typedef struct {// estrutura que cada jogo possui
	char name[10]; //nome do jogo
	char categ[10];//categoria do jogo
	int release, downloads, rating; //lancamento downloads e avaliação
}Game;

Game *listarJogos (Game *a);
Game *listarJogosRating(Game *p);
Game *listarJogosCateg (Game *a);
Game *listarJogosRelease (Game *a);

