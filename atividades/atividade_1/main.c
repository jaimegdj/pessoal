#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define K 3

int main () {
	Game games[K], *p;
	int i, j, flag=1, option;
    	p = games;
    	//
	strcpy((p+0)->name, "Mario");
	strcpy((p+0)->categ, "Aventura");
	(p+0)->release = 89;
	(p+0)->downloads = 54;
	(p+0)->rating = 10;

	strcpy((p+1)->name, "Top Gear");
	strcpy((p+1)->categ, "Corrida");
	(p+1)->release = 92;
	(p+1)->downloads = 87;
	(p+1)->rating = 15;

	strcpy((p+2)->name, "Sim City");
	strcpy((p+2)->categ, "Simulador");
	(p+2)->release = 90;
	(p+2)->downloads = 23;
	(p+2)->rating = 6;
    
	while(flag){
		printf("1 - Lista jogos.\n");
		printf("2 - Lista jogos por avaliacao.\n");
		printf("3 - Lista jogos por categoria.\n");
		printf("4 - Lista jogos por lancamento.\n");
		printf("5 - Fechar programa.\n");
		scanf("%d", &option);

		switch(option){
			case 1:
				for(i=0; i<K; i++)
					printf("%s\n", (p+i)->name);
			break;

			case 2:
				p = listarJogosRating (p);
				for(i=0; i<K; i++)
				 	printf("%s \t %d \n", (p+i)->name, (p+i)->rating);
			break;

			case 3:
				p = listarJogosCateg (p);
				for(i=0; i<K; i++)
					printf("%s\t %s\n", (p+i)->name, (p+i)->categ);
			break;

			case 4:
				p = listarJogosRelease (p);
				for(i=0; i<K; i++)
					printf("%s \t %d \n", (p+i)->name, (p+i)->release);
			break;

			case 5:
				flag = 0;
			break;
		}
	}

	return 0;
}
