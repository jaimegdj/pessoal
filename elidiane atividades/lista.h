
struct no{
	int dado;
	struct no *prox, *ant;
}

typedef struct no Lista;

//cria lista
Lista* crialista();

//insere na lista
Lista * insere(Lista *l, int x);

//retirar um elemento da lista
Lista* retirar(Lista *l, int valor);

//imprime elementos da lista
void imprime(Lista *l);

//imprime lista invertida
void imprimeinvertido(Lista *l);

//busca elemento
Lista* buscaelemento(Lista *, int valor);
